package com.csddesarrollos.javaappupdater;

import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UtilNGTest {

    private final List<ServerVersion> lsv = new ArrayList<>();

    public UtilNGTest() {
        ServerVersion sv;

        sv = new ServerVersion();
        sv.setUpdateVersion(1);
        sv.setNotify(true);
        sv.setMessage("Test Message Uno");
        sv.setChanges(Arrays.asList(new String[]{"Cambio 1", "Cambio 2", "Cambio 3"}));
        lsv.add(sv);

        sv = new ServerVersion();
        sv.setUpdateVersion(2);
        sv.setNotify(true);
        sv.setMessage("Test Message Dos");
        sv.setChanges(Arrays.asList(new String[]{"Cambio 2", "Cambio 3", "Cambio 4"}));
        lsv.add(sv);

        sv = new ServerVersion();
        sv.setUpdateVersion(3);
        sv.setNotify(true);
        sv.setMessage("Test Message Tres");
        sv.setChanges(Arrays.asList(new String[]{"Cambio 3", "Cambio 4", "Cambio 5"}));
        lsv.add(sv);
    }

    /**
     * Se asegura que la actualización suba de 1 en 1 la versión a actualizar.
     */
    @Test
    public void testGetUpdateVersion() {
        System.out.println("getUpdateVersion");

        //De v1 a v2
        ServerVersion expResult1 = new ServerVersion();
        expResult1.setChanges(Arrays.asList(new String[]{"Cambio 2", "Cambio 3", "Cambio 4"}));
        expResult1.setMessage("Test Message Dos");
        expResult1.setNotify(true);
        expResult1.setUpdateVersion(2);

        ServerVersion result1 = Util.getUpdateVersion(lsv, 1);
        Assert.assertEquals(result1.getChanges(), expResult1.getChanges());
        Assert.assertEquals(result1.getMessage(), expResult1.getMessage());
        Assert.assertEquals(result1.getUpdateVersion(), expResult1.getUpdateVersion());
        Assert.assertEquals(result1.isNotify(), expResult1.isNotify());

        //De v2 a v2
        ServerVersion result2 = Util.getUpdateVersion(lsv, 2);

        ServerVersion expResult2 = new ServerVersion();
        expResult2.setChanges(Arrays.asList(new String[]{"Cambio 3", "Cambio 4", "Cambio 5"}));
        expResult2.setMessage("Test Message Tres");
        expResult2.setNotify(true);
        expResult2.setUpdateVersion(3);

        Assert.assertEquals(result2.getChanges(), expResult2.getChanges());
        Assert.assertEquals(result2.getMessage(), expResult2.getMessage());
        Assert.assertEquals(result2.getUpdateVersion(), expResult2.getUpdateVersion());
        Assert.assertEquals(result2.isNotify(), expResult2.isNotify());

        //De v3 a v3
        ServerVersion result3 = Util.getUpdateVersion(lsv, 3);

        ServerVersion expResul3 = new ServerVersion();
        expResul3.setChanges(Arrays.asList(new String[]{"Cambio 3", "Cambio 4", "Cambio 5"}));
        expResul3.setMessage("Test Message Tres");
        expResul3.setNotify(true);
        expResul3.setUpdateVersion(3);

        Assert.assertEquals(result3.getChanges(), expResul3.getChanges());
        Assert.assertEquals(result3.getMessage(), expResul3.getMessage());
        Assert.assertEquals(result3.getUpdateVersion(), expResul3.getUpdateVersion());
        Assert.assertEquals(result3.isNotify(), expResul3.isNotify());
    }

    /**
     * Se asegura de validar correctamente si el número es entero o no.
     */
    @Test
    public void testIsEntero() {
        System.out.println("isEntero");
        String[] s = {"1", null, "5.0", "5.1", "otracosa", "25"};
        boolean expResult[] = {true, false, false, false, false, true};
        boolean result;
        for (int i = 0; i < s.length; i++) {
            result = Util.isEntero(s[i]);
            Assert.assertEquals(result, expResult[i]);
        }
    }

    /**
     * Se asegura de validar correctamente si el valor es numérico o no
     */
    @Test
    public void testIsNumber() {
        System.out.println("isNumber");
        String[] s = {"1.89434234423", null, "5.0", "5", "otracosa", "Otrcosa123"};
        boolean expResult[] = {true, false, true, true, false, false};
        boolean result;
        for (int i = 0; i < s.length; i++) {
            result = Util.isNumber(s[i]);
            Assert.assertEquals(result, expResult[i]);
        }
    }

    /**
     * Se asegura de estar separando correctamente los caracteres numéricos y
     * las letras
     */
    @Test
    public void testSepararNumeroTipo() {
        System.out.println("separarNumeroTipo");
        String[] valor = {"1.5K", "1.5M", "1.5G", "1.5T", "t1.5", "otracosa123", null};
        String[][] expResult = {
            {"1.5", "K"},
            {"1.5", "M"},
            {"1.5", "G"},
            {"1.5", "T"},
            {"1.5", "t"},
            {"123", "otracosa"},
            {"", ""}
        };
        String[] result;
        for (int i = 0; i < valor.length; i++) {
            result = Util.separarNumeroTipo(valor[i]);
            for (int r = 0; r < result.length; r++) {
                Assert.assertEquals(result[r], expResult[i][r]);
            }
        }
    }

    /**
     * Se asegura de estar realizando la conversión correcta entre tamaños.
     */
    @Test
    public void testGetSizeOnKb() {
        System.out.println("getSizeOnKb");
        String[] group = {"22.43K", "521.245M", "0.00M", "estono", "estotampoco", null};
        BigDecimal[] expResult = {
            new BigDecimal("22.43"),
            new BigDecimal("533754.88"),
            new BigDecimal("0"),
            BigDecimal.ZERO,
            BigDecimal.ZERO,
            BigDecimal.ZERO
        };
        BigDecimal result;
        for (int i = 0; i < group.length; i++) {
            result = Util.getSizeOnKb(group[i]);
            Assert.assertEquals(result, expResult[i].setScale(2, RoundingMode.HALF_UP));
        }
    }

}
