package com.csddesarrollos.javaappupdater.autoUpdate.clientSide;

import com.csddesarrollos.javaappupdater.Util;
import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class CurrentVersionNGTest {

    private final File modFile = new File("modifiedFile.xml");

    public CurrentVersionNGTest() throws Exception {
        modFile.deleteOnExit();
        CurrentVersion.getInstance(new File("resources" + File.separator + "AutoUpdate.xml"));
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        CurrentVersion.deleteInstance();
    }

    /**
     * Se asegura de crear solamente una instancia para la configuracion actual
     * de actualización.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetInstance() throws Exception {
        System.out.println("getInstance");
        CurrentVersion expResult = CurrentVersion.getInstance();
        CurrentVersion result = CurrentVersion.getInstance();
        Assert.assertEquals(result, expResult);
    }

    /**
     * Se asegura de guardar correctamente la configuración actual de la
     * configuración.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSaveCurrentVersion() throws Exception {
        System.out.println("saveCurrentVersion");

        CurrentVersion instance = CurrentVersion.getInstance(new File("resources" + File.separator + "AutoUpdate.xml"));

        List<SyncFiles> sfs = new ArrayList<>();
        SyncFiles sf;
        for (int i = 1; i < 4; i++) {
            sf = new SyncFiles();
            sf.setName("File" + i + ".txt");
            sf.setSize(new BigDecimal("9999.99"));
            sf.setLastUpdate(LocalDateTime.parse("2017-01-01 00:00:00", Util.formatter));
            sfs.add(sf);
        }

        instance.setAutoUpdate(false);
        instance.setDestPath(new File("modified"));
        instance.setListedFiles(sfs);
        instance.setMainJar(new File("mainJar.jar"));
        instance.setSrcUrl("srcUrl.com");
        instance.setUpdateVersion(9999);
        CurrentVersion.getInstance(modFile);
        instance.saveCurrentVersion();

        StringBuilder xml = new StringBuilder();
        try (FileReader fr = new FileReader(modFile); BufferedReader br = new BufferedReader(fr)) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                xml.append(sCurrentLine);
            }
        }

        String expResult = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><AutoUpdate><Version><updateVersion>9999</updateVersion><srcUrl>srcUrl.com/</srcUrl><destPath>modified</destPath><autoUpdate>false</autoUpdate><mainJar>mainJar.jar</mainJar><listedFiles><SyncFiles lastUpdate=\"2017-01-01 00:00:00\" name=\"File1.txt\" size=\"9999.99\"/><SyncFiles lastUpdate=\"2017-01-01 00:00:00\" name=\"File2.txt\" size=\"9999.99\"/><SyncFiles lastUpdate=\"2017-01-01 00:00:00\" name=\"File3.txt\" size=\"9999.99\"/></listedFiles></Version></AutoUpdate>";

        Assert.assertEquals(xml.toString(), expResult);
    }

}
