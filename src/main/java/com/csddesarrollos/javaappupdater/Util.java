package com.csddesarrollos.javaappupdater;

import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.swing.JFileChooser;

public class Util {

    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static ServerVersion getUpdateVersion(List<ServerVersion> sv, int currentUpdateVersion) {
        ServerVersion updateVersion = null;
        sv.sort((a, b) -> Integer.compare(b.getUpdateVersion(), a.getUpdateVersion()));
        for (ServerVersion ver : sv) {
            if (ver.getUpdateVersion() == (currentUpdateVersion + 1) || ver.getUpdateVersion() == currentUpdateVersion) {
                updateVersion = ver;
                break;
            }
        }
        return updateVersion;
    }

    public static boolean isEntero(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isNumber(String s) {
        try {
            new BigDecimal(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String dirChooser(String dir) {
        if (dir != null && !dir.trim().equals("") && new File(dir).exists()) {
            dir = dir.trim();
        } else {
            dir = System.getProperty("user.home");
        }
        String path;
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setCurrentDirectory(new File(dir));
        int res = fc.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            path = fc.getSelectedFile().getAbsolutePath();
            if (!path.endsWith(File.separator)) {
                path += File.separator;
            }
        } else {
            path = null;
        }
        return path;
    }

    public static String[] separarNumeroTipo(String valor) {
        String[] sf = new String[2];
        sf[0] = "";
        sf[1] = "";
        if (valor != null) {
            for (int i = 0; i < valor.length(); i++) {
                char c = valor.charAt(i);
                try {
                    Integer.parseInt(Character.toString(c));
                    sf[0] += c;
                } catch (NumberFormatException e) {
                    if (Character.toString(c).equals(".")) {
                        sf[0] += c;
                    } else {
                        sf[1] += c;
                    }
                }
            }
        }
        return sf;
    }

    public static BigDecimal getSizeOnKb(String group) {
        String[] numTipo = Util.separarNumeroTipo(group);
        BigDecimal num;
        if (Util.isNumber(numTipo[0].trim())) {
            switch (numTipo[1].trim()) {
                case "K":
                    num = new BigDecimal(numTipo[0]);
                    break;
                case "M":
                    num = new BigDecimal(numTipo[0]).multiply(new BigDecimal("1024"));
                    break;
                case "G":
                    num = new BigDecimal(numTipo[0]).multiply(new BigDecimal("1024")).multiply(new BigDecimal("1024"));
                    break;
                default:
                    num = new BigDecimal(numTipo[0]);
            }
        } else {
            num = BigDecimal.ZERO;
        }
        return num.setScale(2, RoundingMode.HALF_UP);
    }

}
