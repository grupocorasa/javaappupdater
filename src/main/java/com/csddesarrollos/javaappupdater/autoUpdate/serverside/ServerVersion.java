package com.csddesarrollos.javaappupdater.autoUpdate.serverside;

import com.csddesarrollos.javaappupdater.autoUpdate.SyncConfiguration;
import java.util.ArrayList;
import java.util.List;

public class ServerVersion extends SyncConfiguration {

    private List<String> changes;
    private String message;
    private boolean notify;

    public List<String> getChanges() {
        return changes;
    }

    public void addChange(String ch) {
        if (this.changes == null) {
            this.changes = new ArrayList<>();
        }
        this.changes.add(ch);
    }

    public void setChanges(List<String> changes) {
        this.changes = changes;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isNotify() {
        return notify;
    }

    public void setNotify(boolean notify) {
        this.notify = notify;
    }

}
