package com.csddesarrollos.javaappupdater.autoUpdate.serverside;

import com.csddesarrollos.javaappupdater.Main;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ListServerVersions {

    private final String file = "ServerVersions.xml";
    private final List<ServerVersion> verList;

    public ListServerVersions(String rootUpdate) throws MalformedURLException, IOException, ParserConfigurationException, SAXException {
        Main.dd.setText("Validando si existen nuevas versiones en internet.");
        String finalUrl = rootUpdate + file;
        URL url = new URL(finalUrl);
        URLConnection connection = url.openConnection();
        try (InputStream inputStream = connection.getInputStream()) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputStream);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("Version");
            verList = new ArrayList<>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                verList.add(getVersion(nodeList.item(i)));
            }
        }
    }

    private ServerVersion getVersion(Node node) {
        ServerVersion ver = new ServerVersion();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            ver.setUpdateVersion(Integer.parseInt(element.getAttribute("updateVersion")));

            NodeList nl = element.getElementsByTagName("changes");
            for (int i = 0; i < nl.getLength(); i++) {
                ver.addChange(nl.item(i).getTextContent());
            }

            ver.setMessage(element.getAttribute("message"));
            ver.setNotify(element.getAttribute("notify").equals("true"));
        }
        return ver;
    }

    public List<ServerVersion> getVerList() {
        return verList;
    }

    public static void writeServerVersions(File f, List<ServerVersion> verList) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Element rootElement = doc.createElement("AutoUpdate");
        doc.appendChild(rootElement);

        Element staff;
        Attr attr;
        for (ServerVersion v : verList) {
            staff = doc.createElement("Version");
            rootElement.appendChild(staff);

            attr = doc.createAttribute("updateVersion");
            attr.setValue(String.valueOf(v.getUpdateVersion()));
            staff.setAttributeNode(attr);

            for (String s : v.getChanges()) {
                Element firstname = doc.createElement("changes");
                firstname.appendChild(doc.createTextNode(s));
                staff.appendChild(firstname);
            }

            attr = doc.createAttribute("notify");
            attr.setValue(v.isNotify() ? "true" : "false");
            staff.setAttributeNode(attr);

            attr = doc.createAttribute("message");
            attr.setValue(v.getMessage());
            staff.setAttributeNode(attr);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
    }

}
