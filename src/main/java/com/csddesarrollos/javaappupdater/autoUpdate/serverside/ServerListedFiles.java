package com.csddesarrollos.javaappupdater.autoUpdate.serverside;

import com.csddesarrollos.javaappupdater.Util;
import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerListedFiles {

    public static List<SyncFiles> exploreFolder(String mainUrl, String dirUrl) throws MalformedURLException, IOException, InterruptedException {
        URL url = new URL(mainUrl + dirUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("User-Agent", "");
        BufferedInputStream stream = new BufferedInputStream(connection.getInputStream());

        int available;
        String html = "";
        do {
            available = stream.available();
            byte[] b = new byte[available];
            stream.read(b);
            html += new String(b);
            Thread.sleep(100);
        } while (!html.endsWith("</html>\n"));

        List<SyncFiles> l = new ArrayList<>();
        SyncFiles sf;
        String aux;
        Pattern p;
        Matcher m;

        p = Pattern.compile("<a href=\"(.*?)\">(.*)(<td align=\"right\">([0-9]{4}.*?)</td>)(.*)(<td align=\"right\">(.*?)</td>)");
        m = p.matcher(html);
        while (m.find()) {
            aux = m.group(1);
            if (!aux.startsWith("?") && !aux.startsWith("/")) {
                if (aux.endsWith("/")) {
                    l.addAll(exploreFolder(mainUrl.trim(), dirUrl + aux.trim()));
                } else {
                    LocalDateTime date = LocalDateTime.parse(m.group(4).trim() + ":00", Util.formatter);
                    sf = new SyncFiles();
                    sf.setName(dirUrl + aux.trim());
                    sf.setLastUpdate(date);
                    sf.setSize(Util.getSizeOnKb(m.group(7).trim()));
                    l.add(sf);
                }
            }
        }
        return l;
    }

}
