package com.csddesarrollos.javaappupdater.autoUpdate.clientSide;

import com.csddesarrollos.javaappupdater.Main;
import com.csddesarrollos.javaappupdater.Util;
import com.csddesarrollos.javaappupdater.autoUpdate.SyncConfiguration;
import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ListServerVersions;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerListedFiles;
import java.awt.Desktop;
import java.awt.Dimension;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CurrentVersion extends SyncConfiguration {

    private static CurrentVersion instance;

    private static File f = new File("AutoUpdate.xml");
    private String srcUrl;
    private File destPath;
    private boolean autoUpdate;
    private File mainJar;
    private List<SyncFiles> listedFiles;

    public static CurrentVersion getInstance() throws URISyntaxException, IOException, FileNotFoundException, MalformedURLException, ParserConfigurationException, SAXException, InterruptedException, TransformerException {
        if (instance == null) {
            Main.dd.setText("Instanciando la configuración local");
            instance = new CurrentVersion();
        }
        return instance;
    }

    private CurrentVersion() throws URISyntaxException, IOException, FileNotFoundException, MalformedURLException, ParserConfigurationException, SAXException, InterruptedException, TransformerException {
        if (!f.exists()) {
            Main.dd.setVisible(false);
            createNewConfig();
            Main.dd.setVisible(true);
        } else {
            Main.dd.setText("Leyendo archivo actual de configuración.");
            readConfig();
        }
    }

    private void createNewConfig() throws URISyntaxException, IOException, MalformedURLException, ParserConfigurationException, SAXException, InterruptedException, TransformerException {
        String aux;

        JOptionPane.showMessageDialog(
                null,
                "Seleccione la ruta del servidor para la actualización automática.",
                "Seleccione la ruta",
                JOptionPane.INFORMATION_MESSAGE
        );
        try (InputStream is = getClass().getResourceAsStream("/defaultValues.properties")) {
            if (is != null) {
                Properties p = new Properties();
                p.load(is);
                String defaultUrl = p.getProperty("default.server.url");
                Desktop.getDesktop().browse(new URI(defaultUrl));
            }
        }
        setSrcUrl(JOptionPane.showInputDialog(null, "Capture la ruta raiz del servidor de actualización."));

        ListServerVersions lsv = new ListServerVersions(getSrcUrl());
        Integer[] vers = lsv.getVerList().stream()
                .map(l -> (Integer) l.getUpdateVersion())
                .toArray(Integer[]::new);
        int resp = JOptionPane.showOptionDialog(
                null,
                "Seleccione la versión de actualización con la que desea comenzar.",
                "Selección de versión",
                JOptionPane.WARNING_MESSAGE,
                0,
                null,
                vers,
                vers[vers.length - 1]
        );
        resp = vers[resp];
        setUpdateVersion(resp);

        int au = JOptionPane.showConfirmDialog(
                null,
                "Desea activar las actualizaciones automáticas?",
                "Confirmación",
                JOptionPane.YES_NO_OPTION
        );
        setAutoUpdate(au == JOptionPane.YES_OPTION);

        do {
            JOptionPane.showMessageDialog(
                    null,
                    "Seleccione la carpeta raiz en donde desea que se descargue y ejecute el sistema.",
                    "Confirmación",
                    JOptionPane.WARNING_MESSAGE
            );
            aux = Util.dirChooser(null);
            if (aux != null) {
                setDestPath(new File(aux));
            }
        } while (!new File(aux).exists());

        String[] slf = ServerListedFiles.exploreFolder(getSrcUrl() + "v" + resp + "/", "").stream()
                .map(fl -> fl.getName())
                .toArray(String[]::new);
        JList list = new JList(slf);
        list.setSelectedIndex(0);
        JScrollPane scrollpane = new JScrollPane(list);
        scrollpane.setPreferredSize(new Dimension(300, 125));
        JOptionPane.showMessageDialog(
                null,
                scrollpane,
                "Seleccione el archivo Main que desea que se ejecute al terminar la actualización.",
                JOptionPane.WARNING_MESSAGE
        );
        setMainJar(new File(list.getSelectedValue().toString()));

        saveCurrentVersion();
    }

    private void readConfig() throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
        try (FileInputStream fis = new FileInputStream(f)) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fis);
            doc.getDocumentElement().normalize();

            NodeList nl;
            String aux;

            nl = doc.getElementsByTagName("updateVersion");
            aux = nl.item(0).getTextContent();
            setUpdateVersion(Integer.parseInt(aux));

            nl = doc.getElementsByTagName("srcUrl");
            aux = nl.item(0).getTextContent();
            setSrcUrl(aux);

            nl = doc.getElementsByTagName("destPath");
            aux = nl.item(0).getTextContent();
            setDestPath(new File(aux));

            nl = doc.getElementsByTagName("autoUpdate");
            aux = nl.item(0).getTextContent();
            setAutoUpdate(aux.equals("true"));

            nl = doc.getElementsByTagName("mainJar");
            aux = nl.item(0).getTextContent();
            setMainJar(new File(aux));

            nl = doc.getElementsByTagName("SyncFiles");
            for (int i = 0; i < nl.getLength(); i++) {
                addListedFiles(getSyncFile(nl.item(i)));
            }
        }
    }

    private SyncFiles getSyncFile(Node node) {
        Main.dd.setText("Leyendo archivos sincronizados del archivo de configuración.");
        SyncFiles sf = new SyncFiles();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;

            sf.setName(element.getAttribute("name"));

            LocalDateTime date = LocalDateTime.parse(element.getAttribute("lastUpdate").trim(), Util.formatter);
            sf.setLastUpdate(date);

            sf.setSize(new BigDecimal(element.getAttribute("size")));
        }
        return sf;
    }

    public void saveCurrentVersion() throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        Main.dd.setText("Escribiendo archivo de configuración actual.");
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Element root = doc.createElement("AutoUpdate");
        doc.appendChild(root);

        Element ver = doc.createElement("Version");
        root.appendChild(ver);

        Element aux;

        aux = doc.createElement("updateVersion");
        aux.appendChild(doc.createTextNode(String.valueOf(getUpdateVersion())));
        ver.appendChild(aux);

        aux = doc.createElement("srcUrl");
        aux.appendChild(doc.createTextNode(getSrcUrl()));
        ver.appendChild(aux);

        aux = doc.createElement("destPath");
        aux.appendChild(doc.createTextNode(getDestPath().getPath()));
        ver.appendChild(aux);

        aux = doc.createElement("autoUpdate");
        aux.appendChild(doc.createTextNode(isAutoUpdate() ? "true" : "false"));
        ver.appendChild(aux);

        aux = doc.createElement("mainJar");
        aux.appendChild(doc.createTextNode(getMainJar().getPath()));
        ver.appendChild(aux);

        aux = doc.createElement("listedFiles");
        Element fileElem;
        Attr attr;
        if (getListedFiles() != null) {
            for (SyncFiles sf : getListedFiles()) {
                fileElem = doc.createElement("SyncFiles");

                attr = doc.createAttribute("name");
                attr.setValue(sf.getName());
                fileElem.setAttributeNode(attr);

                attr = doc.createAttribute("lastUpdate");
                attr.setValue(sf.getLastUpdate().format(Util.formatter));
                fileElem.setAttributeNode(attr);

                attr = doc.createAttribute("size");
                attr.setValue(sf.getSize().setScale(2, RoundingMode.HALF_UP).toString());
                fileElem.setAttributeNode(attr);

                aux.appendChild(fileElem);
            }
            ver.appendChild(aux);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
    }

    public String getSrcUrl() {
        return srcUrl;
    }

    public void setSrcUrl(String srcUrl) {
        if (!srcUrl.endsWith("/")) {
            srcUrl += "/";
        }
        this.srcUrl = srcUrl;
    }

    public File getDestPath() {
        return destPath;
    }

    public void setDestPath(File destPath) {
        this.destPath = destPath;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    public File getMainJar() {
        return mainJar;
    }

    public void setMainJar(File mainJar) {
        this.mainJar = mainJar;
    }

    public List<SyncFiles> getListedFiles() {
        return listedFiles;
    }

    public void addListedFiles(SyncFiles f) {
        if (this.listedFiles == null) {
            this.listedFiles = new ArrayList<>();
        }
        this.listedFiles.add(f);
    }

    public void setListedFiles(List<SyncFiles> listedFiles) {
        this.listedFiles = listedFiles;
    }

    /**
     * Este método solo debería ser usado para los tests
     *
     * @param f
     * @return
     * @throws URISyntaxException
     * @throws IOException
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws InterruptedException
     * @throws TransformerException
     * @deprecated
     */
    @Deprecated
    public static CurrentVersion getInstance(File f) throws URISyntaxException, IOException, FileNotFoundException, MalformedURLException, ParserConfigurationException, SAXException, InterruptedException, TransformerException {
        CurrentVersion.f = f;
        if (instance == null) {
            instance = new CurrentVersion();
        }
        return instance;
    }

    /**
     *
     * Este método solo debería ser usado para los tests
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws FileNotFoundException
     * @throws MalformedURLException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws InterruptedException
     * @throws TransformerException
     * @deprecated
     */
    @Deprecated
    public static void deleteInstance() throws URISyntaxException, IOException, FileNotFoundException, MalformedURLException, ParserConfigurationException, SAXException, InterruptedException, TransformerException {
        instance = null;
    }

}
