package com.csddesarrollos.javaappupdater.autoUpdate;

public class SyncConfiguration {

    private int updateVersion;

    public int getUpdateVersion() {
        return updateVersion;
    }

    public void setUpdateVersion(int updateVersion) {
        this.updateVersion = updateVersion;
    }

}
