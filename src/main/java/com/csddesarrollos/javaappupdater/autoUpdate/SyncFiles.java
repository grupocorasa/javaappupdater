package com.csddesarrollos.javaappupdater.autoUpdate;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class SyncFiles {

    private String name;
    private LocalDateTime lastUpdate;
    private BigDecimal size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

}
