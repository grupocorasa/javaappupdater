package com.csddesarrollos.javaappupdater.download;

import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.clientSide.CurrentVersion;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerListedFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

public class SameVersion extends Download {

    public SameVersion(CurrentVersion cv, ServerVersion lv) {
        super(cv, lv);
    }

    @Override
    public List<SyncFiles> getSyncedFiles() throws IOException, MalformedURLException, InterruptedException {
        List<SyncFiles> serverFiles = ServerListedFiles.exploreFolder(cv.getSrcUrl() + "v" + cv.getUpdateVersion() + "/", "");
        Iterator it = serverFiles.iterator();
        while (it.hasNext()) {
            SyncFiles sf = (SyncFiles) it.next();
            for (SyncFiles cvf : cv.getListedFiles()) {
                if (cvf.getName().equals(sf.getName())) {
                    if (cvf.getLastUpdate().compareTo(sf.getLastUpdate()) == 0) {
                        it.remove();
                        break;
                    }
                }
            }
        }
        return serverFiles;
    }

}
