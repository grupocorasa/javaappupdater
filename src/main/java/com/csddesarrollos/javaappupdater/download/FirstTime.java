package com.csddesarrollos.javaappupdater.download;

import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.clientSide.CurrentVersion;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerListedFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.io.IOException;
import java.util.List;

public class FirstTime extends Download {

    public FirstTime(CurrentVersion cv, ServerVersion lv) {
        super(cv, lv);
    }

    @Override
    public List<SyncFiles> getSyncedFiles() throws IOException, InterruptedException {
        if (cv.getSrcUrl().toLowerCase().contains("test")) {
            return ServerListedFiles.exploreFolder(cv.getSrcUrl(), "");
        } else {
            return ServerListedFiles.exploreFolder(cv.getSrcUrl() + "v" + cv.getUpdateVersion() + "/", "");
        }
    }

}
