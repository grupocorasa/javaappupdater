package com.csddesarrollos.javaappupdater.download;

import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.clientSide.CurrentVersion;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerListedFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public class LatestVersion extends Download {

    public LatestVersion(CurrentVersion cv, ServerVersion latestVersion) {
        super(cv, latestVersion);
    }

    @Override
    public List<SyncFiles> getSyncedFiles() throws IOException, MalformedURLException, InterruptedException {
        return ServerListedFiles.exploreFolder(cv.getSrcUrl() + "v" + lv.getUpdateVersion() + "/", "");
    }

}
