package com.csddesarrollos.javaappupdater.download;

import com.csddesarrollos.javaappupdater.Main;
import com.csddesarrollos.javaappupdater.autoUpdate.SyncFiles;
import com.csddesarrollos.javaappupdater.autoUpdate.clientSide.CurrentVersion;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

public abstract class Download {

    public final CurrentVersion cv;
    public final ServerVersion lv;
    public boolean exito = false;

    public Download(CurrentVersion cv, ServerVersion lv) {
        this.cv = cv;
        this.lv = lv;
    }

    public void Download() throws IOException, MalformedURLException, InterruptedException {
        try {
            deleteFiles();
            File temp;
            Main.dd.setText("Comenzando descarga de nuevo archivos.");
            for (SyncFiles sf : getSyncedFiles()) {
                Main.dd.setText("Descargando archivo: " + sf.getName());
                URL website;
                if (cv.getSrcUrl().toLowerCase().contains("test")) {
                    website = new URL(cv.getSrcUrl() + sf.getName());
                } else {
                    website = new URL(cv.getSrcUrl() + "v" + cv.getUpdateVersion() + "/" + sf.getName());
                }
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());
                temp = new File(cv.getDestPath() + File.separator + sf.getName());
                temp.getParentFile().mkdirs();
                FileOutputStream fos = new FileOutputStream(temp.getPath());
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                Main.dd.setText("Archivo descargado correctamente: " + sf.getName());
            }
            Main.dd.setText("Descarga terminada exitosamente.");
            exito = true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(
                    null,
                    "La descarga será interrumpida debido a un error: " + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
            e.printStackTrace();
            File temp;
            for (SyncFiles sf : getSyncedFiles()) {
                temp = new File(cv.getDestPath() + File.separator + sf.getName());
                if (temp.exists()) {
                    temp.delete();
                }
            }
            exito = false;
        }
    }

    public void updateCurrentVersion() throws ParserConfigurationException, TransformerException, IOException, MalformedURLException, InterruptedException {
        List<SyncFiles> sfs = getSyncedFiles();
        List<String> updated = sfs.stream()
                .map(fs -> fs.getName())
                .collect(Collectors.toList());
        if (cv.getListedFiles() != null) {
            cv.getListedFiles().removeIf(f -> updated.contains(f.getName()));
        }
        sfs.stream().forEach(f -> cv.addListedFiles(f));
        cv.saveCurrentVersion();
    }

    private void deleteFiles() throws IOException, MalformedURLException, InterruptedException {
        Main.dd.setText("Eliminando archivos que se van a actualizar.");
        File aux;
        for (SyncFiles f : getSyncedFiles()) {
            aux = new File(cv.getDestPath() + File.separator + f.getName());
            if (aux.exists()) {
                aux.delete();
            }
        }
        Main.dd.setText("Archivos eliminados correctamente.");
    }

    public abstract List<SyncFiles> getSyncedFiles() throws IOException, MalformedURLException, InterruptedException;

}
