package com.csddesarrollos.javaappupdater;

import com.csddesarrollos.javaappupdater.autoUpdate.clientSide.CurrentVersion;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ListServerVersions;
import com.csddesarrollos.javaappupdater.autoUpdate.serverside.ServerVersion;
import com.csddesarrollos.javaappupdater.download.Download;
import com.csddesarrollos.javaappupdater.download.FirstTime;
import com.csddesarrollos.javaappupdater.download.LatestVersion;
import com.csddesarrollos.javaappupdater.download.SameVersion;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

public class Main {

    public static DescargandoDialog dd;

    public static void main(String[] args) {
        dd = new DescargandoDialog(null, false);
        dd.setAlwaysOnTop(true);
        dd.setLocationRelativeTo(null);
        dd.setVisible(true);
        dd.setText("Comenzando actualización.");

        CurrentVersion cv = null;
        try {
            cv = CurrentVersion.getInstance();
        } catch (URISyntaxException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración local no tiene la sintaxis de una liga al servidor: " + ex.getMessage() + "\nPor Ejemplo: \nhttp://miservidor/micarpeta",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración local no pudo ser leido correctamente: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ParserConfigurationException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración local no puede ser tratado como tal ya que no cuenta con dicha estructura: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (SAXException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al estructurar el archivo de configuración local: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al esperar a que termine de cargar la página: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (TransformerException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración local no puede ser parseado a XML: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        if (cv == null) {
            System.exit(1);
        }

        ListServerVersions sv = null;
        try {
            sv = new ListServerVersions(cv.getSrcUrl());
            sv.getVerList().sort((a, b) -> Integer.compare(b.getUpdateVersion(), a.getUpdateVersion()));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración web no pudo ser leido correctamente: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ParserConfigurationException ex) {
            JOptionPane.showMessageDialog(null,
                    "El archivo de configuración web no puede ser tratado como tal ya que no cuenta con dicha estructura: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (SAXException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al estructurar el archivo de configuración web: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        if (sv == null) {
            System.exit(1);
        }

        Main.dd.setText("Validando si es una instalación limpia.");
        boolean firstTime = cv.getListedFiles() == null || cv.getListedFiles().isEmpty();

        Download download = null;
        try {
            if (firstTime) {
                Main.dd.setText("Primera ejecución detectada, descargará todos los archivos actuales.");
                download = new FirstTime(cv, null);
                download.Download();
            } else if (cv.isAutoUpdate()) {
                Main.dd.setText("Validando actualización de manera automática.");
                ServerVersion updateVersion = Util.getUpdateVersion(sv.getVerList(), cv.getUpdateVersion());
                if (updateVersion != null && cv.getUpdateVersion() < updateVersion.getUpdateVersion()) {
                    Main.dd.setText("Actualizando archivos de la siguiente versión.");
                    if (updateVersion.isNotify()) {
                        if (updateNewVersion(cv, updateVersion)) {
                            download = new LatestVersion(cv, updateVersion);
                            download.Download();
                        }
                    } else {
                        download = new LatestVersion(cv, updateVersion);
                        download.Download();
                    }
                } else if (updateVersion != null && cv.getUpdateVersion() == updateVersion.getUpdateVersion()) {
                    Main.dd.setText("Actualizando archivos de la misma versión.");
                    download = new SameVersion(cv, null);
                    download.Download();
                }
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al leer los archivos del servidor para descargar: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al esperar a que termine de cargar la página al descargar: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

        try {
            Main.dd.setText("Actualizando información de configuración local.");
            if (download != null && download.exito) {
                download.updateCurrentVersion();
            }
        } catch (ParserConfigurationException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al escribir la configuración local, no puede ser tratado como tal ya que no cuenta con dicha estructura: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (TransformerException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al parsear de objeto a XML al momento de intentar escribir los archivos descargados: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al leer los archivos del servidor para descargar: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al esperar a que termine de cargar la página al descargar los archivos: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

        Runtime rt = Runtime.getRuntime();
        try {
            Main.dd.setText("Ejecutando el archivo indicado.");
            rt.exec("java -jar " + cv.getMainJar(), null, cv.getDestPath());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Error al ejecutar el archivo de Main Principal: " + ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        System.exit(0);
    }

    private static boolean updateNewVersion(CurrentVersion cv, ServerVersion updateVersion) {
        StringBuilder sb = new StringBuilder();
        sb.append("Está seguro que desea realizar la actualización?\n")
                .append("Actulmente se encuentra en la versión comercial ")
                .append("v")
                .append(cv.getUpdateVersion())
                .append(" y está por actualizar a la versión ")
                .append("v")
                .append(updateVersion.getUpdateVersion());
        sb.append("\n")
                .append(updateVersion.getMessage());
        if (updateVersion.getChanges() != null && !updateVersion.getChanges().isEmpty()) {
            sb.append("\n-")
                    .append(String.join("\n", updateVersion.getChanges()));
        }
        int accept = JOptionPane.showConfirmDialog(
                null,
                sb.toString(),
                "Aviso!",
                JOptionPane.YES_NO_OPTION
        );
        return accept == JOptionPane.YES_OPTION;
    }

}
