Java Update Application
====================

## Descripción
Este proyecto nace de la necesidad de poder actualizar de manera automática una aplicación de escritorio Java. Al no encontrar un proyecto "amigable" para hacerlo, nos hicimos a la tarea de desarrollar uno.

## Requerimientos
* Servidor Web con Apache 2
* Conocimiento Básicos en archivos XML

## Instrucciones de Uso
* Requerimos designar una carpeta "raiz" en nuestro servidor apache2, donde concentraremos toda la información a partir de ahí.
* Una vez definida la carpeta, hay que crear un archivo como el que se encuentra en [resources/ServerVersions.xml] (con el mismo nombre), para especificar el número de las versiones que estan disponibles.
* Hay que subir el archivo ServerVersions.xml en dicha carpeta.
* Hay que crear subcarpetas según el número de versiones que hayamos colocado anteponiendo la letra "v" (por ejemplo: http://miservidor.com/micarpetaraiz/v1/)
* Dentro de estas subcarpetas hay que colocar TODO lo que queremos se actualice.
* Inicializar la aplicación con este proyecto, en donde nos pedirá configurar todas las rutas del servidor y localmente para hacer la actualización, y ejecución.

## Propiedades AutoUpdate.xml
* [UpdateVersion] La versión de actualización que corresponde los archivos descargados actualmente. [Valor entero]
* [srcUrl] La ruta a la carpeta Raiz del servidor apache2 de nuestra aplicación [Valor alfanumérico]
* [destPath] La ruta en el equipo local en donde correrá el programa, ahí mismo se descargaran los nuevos archivos. [Valor alfanumérico]
* [autoUpdate] Indica si queremos que el sistema revise constantemente las actualizaciones o solo cuando se le indique [Valores permitidos: "true", "false"]
* [mainJar] Indica cual será el archivo principal que deseamos ejecutar una vez terminado el proceso (partiendo desde [destPath]. [Valor alfanumérico]
* [listedFiles] Indique el listado de archivos descargados y cuales son sus propiedades (recopiladas desde el servidor apache2.
* * [SyncFiles] Cada uno de los archivos que actualmente se encuentran sincronizados.
* * * lastUpdate: La fecha qe indicaba el servidor apache2 cuando se realizó la última actualización.
* * * name: La ruta y nombre del archivo en cuestión (partiendo de la carpeta "raiz" del servidor apache2
* * * size: El tamaño del archivo que el servidor apache2 indicaba.

## Propiedades ServerVersions.xml
* [AutoUpdate] Nodo principal del archivo, solo debe existir uno.
* [Version] Cada nodo de este tipo representará a una versión distinta.
* * message: Indica si queremos desplegar un mensaje al momento de hacer la actualización.
* * notify: Indica si deseamos que se requiere de una autorización por parte de el usuario al momento de actualizar.
* * updateVersion: Indica la versión exacta que representa este nodo.
* [changes] Puede contener hasta N nodos adentro del nodo versión, y nos indica si queremos especificar los principales cambios del sistema para así mostrarlos al usuario.

## Objetivos Finales
Con ayuda de un servidor web, poder actualizar una aplicación Java de manera automática.

### Dudas y comentarios
¿Tienes algún problema o sugerencia de mejora?
hortegag91@gmail.com

### Actualizaciones
* 1.0
* * Inicio del proyecto listo para operación.